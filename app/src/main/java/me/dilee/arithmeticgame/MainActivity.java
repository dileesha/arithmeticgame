package me.dilee.arithmeticgame;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.PopupWindow;


public class MainActivity extends ActionBarActivity {

    private ImageButton btnNewGame, btnContinue, btnAbout, btnExit;
    private String[] diffLevels = {"Novice", "Easy", "Medium", "Guru"};
    Button btnClosePopup;
    AlertDialog.Builder builder;
    AlertDialog.Builder alertDialogBuilder;

    private PopupWindow popupAbout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // initialization of buttons
        btnNewGame = (ImageButton) findViewById(R.id.btn_newgame);
        btnContinue = (ImageButton) findViewById(R.id.btn_continue);

        btnAbout = (ImageButton) findViewById(R.id.btn_about);
        btnExit = (ImageButton) findViewById(R.id.btn_exit);
        builder = new AlertDialog.Builder(this);
        alertDialogBuilder = new AlertDialog.Builder(this);
        initializeButtons();

    }

    /**
     * initializes button click listeners
     */
    private void initializeButtons() {

        btnNewGame.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                builder.setTitle("Difficulty")
                        .setSingleChoiceItems(diffLevels, 0, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int level) {
                                dialog.dismiss();
                                startGame(level);
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        btnContinue.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                resumeGame();
            }
        });


        btnAbout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                initiatePopupWindow();
            }
        });


        btnExit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                // set the title of the Alert Dialog
                alertDialogBuilder.setTitle("Exit Application");

                // set dialog message
                alertDialogBuilder
                        .setMessage("Are you sure?")
                        .setCancelable(false)
                        .setPositiveButton("Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        MainActivity.this.finish();
                                    }
                                })
                        .setNegativeButton("No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            }
        });
    }

    private void initiatePopupWindow() {
        try {
            LayoutInflater inflater = (LayoutInflater) MainActivity.this
                    .getSystemService(getApplicationContext().LAYOUT_INFLATER_SERVICE);
            View layout = inflater.inflate(R.layout.popup,
                    (ViewGroup) findViewById(R.id.popup_element));
            DisplayMetrics metrics = getApplicationContext().getResources().getDisplayMetrics();
            int width = metrics.widthPixels;
            int height = metrics.heightPixels;
            popupAbout = new PopupWindow(layout, width-50, height-100, true);
            popupAbout.showAtLocation(layout, Gravity.CENTER, 0, 0);

            btnClosePopup = (Button) layout.findViewById(R.id.btn_close_popup);
            btnClosePopup.setOnClickListener(cancel_button_click_listener);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private View.OnClickListener cancel_button_click_listener = new View.OnClickListener() {
        public void onClick(View v) {
            popupAbout.dismiss();
        }
    };


    private void startGame(int level) {
        Intent i = new Intent(getApplicationContext(), GameActivity.class);
        i.putExtra("level", level);
        i.putExtra("continue", 0);
        this.startActivity(i);
    }

    private void resumeGame() {
        Intent i = new Intent(getApplicationContext(), GameActivity.class);
        i.putExtra("level", 0);
        i.putExtra("continue", 1);
        this.startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
