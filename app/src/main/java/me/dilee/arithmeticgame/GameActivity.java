package me.dilee.arithmeticgame;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;



public class GameActivity extends ActionBarActivity implements View.OnClickListener {

    // operator classification
    private final int ADD = 0, SUBTRACT = 1, MULTIPLY = 2, DIVIDE = 3;
    private String[] operators = {"+", "-", "x", "/"};

    private ArrayList<String> operatorArray;
    private ArrayList<Integer> operandArray;

    private boolean hintsEnabled, isFinalAnswer, resume;

    private GameState saveState;

    // initial game data
    private int level = 0, answer = 0, attempts = 1, totalScore = 0, questionNumber = 0, timeRemaining = 0;

    String questionString = "";

    private Random random;

    CountDownTimer timer;

    ImageButton btn_1, btn_2, btn_3, btn_4, btn_5, btn_6, btn_7, btn_8, btn_9, btn_0, btn_del, btn_hash, btn_minus;
    Switch switch_hints;
    TextView tvQuestion, tvResult, tvScore, tvTime;

    AlertDialog.Builder alertDialogBuilder;
    AlertDialog.Builder builder;

    public GameActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        // setting the difficulty level
        level = this.getIntent().getIntExtra("level",0);
        System.out.println("Level : " + level);

        resume = false;

        // initialization of components
        btn_0 = (ImageButton) findViewById(R.id.btn_0);
        btn_1 = (ImageButton) findViewById(R.id.btn_1);
        btn_2 = (ImageButton) findViewById(R.id.btn_2);
        btn_3 = (ImageButton) findViewById(R.id.btn_3);
        btn_4 = (ImageButton) findViewById(R.id.btn_4);
        btn_5 = (ImageButton) findViewById(R.id.btn_5);
        btn_6 = (ImageButton) findViewById(R.id.btn_6);
        btn_7 = (ImageButton) findViewById(R.id.btn_7);
        btn_8 = (ImageButton) findViewById(R.id.btn_8);
        btn_9 = (ImageButton) findViewById(R.id.btn_9);
        btn_del = (ImageButton) findViewById(R.id.btn_del);
        btn_hash = (ImageButton) findViewById(R.id.btn_hash);
        btn_minus = (ImageButton) findViewById(R.id.btn_minus);

        tvQuestion = (TextView) findViewById(R.id.tvQuestion);
        tvResult = (TextView) findViewById(R.id.tvResult);
        tvScore = (TextView) findViewById(R.id.tv_score);
        tvTime = (TextView) findViewById(R.id.tv_time);
        switch_hints = (Switch) findViewById(R.id.switch_hints);

        builder = new AlertDialog.Builder(this);
        alertDialogBuilder = new AlertDialog.Builder(this);

        hintsEnabled = false;

        saveState = new GameState();

        timer = new CountDownTimer(11000, 1000) {

            public void onTick(long millisUntilFinished) {
                int timeLeft = (int) (millisUntilFinished / 1000);
                timeRemaining = timeLeft;
                tvTime.setText(timeLeft + "");
            }

            public void onFinish() {
                generateQuestion();
            }
        };

        random = new Random();

        operatorArray = new ArrayList<>();
        operandArray = new ArrayList<>();

        questionNumber = 0;


        btn_hash.setOnClickListener(this);
        btn_del.setOnClickListener(this);
        btn_minus.setOnClickListener(this);
        btn_0.setOnClickListener(this);
        btn_1.setOnClickListener(this);
        btn_2.setOnClickListener(this);
        btn_3.setOnClickListener(this);
        btn_4.setOnClickListener(this);
        btn_5.setOnClickListener(this);
        btn_6.setOnClickListener(this);
        btn_7.setOnClickListener(this);
        btn_8.setOnClickListener(this);
        btn_9.setOnClickListener(this);

        switch_hints.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                                         boolean isChecked) {

                if(isChecked){
                        hintsEnabled = true;
                    attempts = 4;
                        Toast.makeText(getApplicationContext(), "Hints are on", Toast.LENGTH_SHORT).show();
                }else{
                    attempts = 1;
                    hintsEnabled = false;
                    Toast.makeText(getApplicationContext(), "Hints are off", Toast.LENGTH_SHORT).show();
                }

            }
        });

        if(this.getIntent().getIntExtra("continue",0) == 1){
            resumeGame();
        } else {
            generateQuestion();
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void generateQuestion(){

        if(questionNumber == 10){
            Intent i = new Intent(getApplicationContext(), GameOverActivity.class);
            i.putExtra("score", totalScore);
            this.startActivity(i);
            finish();
            return;
        }

        questionNumber++;
        tvResult.setTextColor(Color.WHITE);
        tvResult.setText("Question " + questionNumber+"");
        switch_hints.setEnabled(true);
        switch_hints.setChecked(hintsEnabled);
        operatorArray = new ArrayList<>();
        operandArray = new ArrayList<>();

        if(hintsEnabled){
            attempts = 4;
        } else {
            attempts = 1;
        }

        int expressionLength = 0;
        answer = 0;
        tvQuestion.setText("= ?");
        questionString = "";
        timeRemaining = 0;


        // minimum + random.nextInt() % (maximum - minimum + 1)
        switch (level){
            case 0: expressionLength = 2; break;
            case 1: expressionLength =  2 + random.nextInt(2);break;
            case 2: expressionLength =  2 + random.nextInt(3); break;
            case 3: expressionLength =  4 + random.nextInt(3); break;
        }

        System.out.println("Expression Length : " + expressionLength);

        for(int i = 0; i < expressionLength - 1; i++){
            operatorArray.add(operators[random.nextInt(4)]);
            //System.out.println(operatorArray.get(i));
        }

        for(int i = 0; i < expressionLength; i++){
            switch (level){
                case 0: operandArray.add(1 + random.nextInt(100)); break;
                case 1: operandArray.add(1 + random.nextInt(100));break;
                case 2: operandArray.add(1 + random.nextInt(500)); break;
                case 3: operandArray.add(1 + random.nextInt(1000)); break;
            }
        }

        //System.out.println(1 + random.nextInt(1000));
        for(int i = 0; i < expressionLength - 1; i++){
            if(i == 0){
                answer = calculateSubExpression(operandArray.get(i), operandArray.get(i+1), operatorArray.get(i) );
                questionString += operandArray.get(i) + operatorArray.get(i) + "" + operandArray.get(i + 1);
            } else {
                answer = calculateSubExpression(answer, operandArray.get(i+1), operatorArray.get(i) );
                questionString += operatorArray.get(i) + "" + operandArray.get(i + 1);
            }
        }

        questionString += " = ?";

        tvQuestion.setText(questionString);

        timer.cancel();
        timer.start();

    }

    public int calculateSubExpression(int operand1, int operand2, String operator){

        int answer = 0;

        if(operator.equals("+")){
            answer = Math.round(operand1 + operand2);
        } else if (operator.equals("-")){
            answer = Math.round(operand1 - operand2);
        } else if (operator.equals("/")){
            answer = Math.round(operand1 / operand2);
        }else if (operator.equals("x")){
            answer = Math.round(operand1 * operand2);
        }

        return answer;
    }

    @Override
    public void onClick(View v) {

        if(v.getId()==R.id.btn_hash){

            if(tvResult.getText().toString().equals("CORRECT!")){
                generateQuestion();
                return;
            }

            if(attempts > 0){
                String inputAnswer = tvQuestion.getText().toString();
                int enteredAnswer = 0;

                if(!inputAnswer.endsWith("?") && !inputAnswer.trim().endsWith("="))
                {
                    //System.out.println((inputAnswer.split("=")[1]).trim());
                    enteredAnswer = Integer.parseInt(inputAnswer.split("=")[1].trim());
                }

                System.out.println("Given Answer : " + ((inputAnswer.split("=")[1]).trim()));

                if(enteredAnswer == answer){
                    //updateScore();
                    tvResult.setTextColor(Color.GREEN);
                    tvResult.setText("CORRECT!");

                    if(timeRemaining != 10 ){
                        int score = 100/(10 - timeRemaining);
                        totalScore += score;
                    } else {
                        totalScore = 100;
                    }

                    tvScore.setText(totalScore+"");
                    attempts = 0;

                } else {

                    if(attempts > 1){
                        tvResult.setText((enteredAnswer < answer) ? "Greater" : "Less");
                        attempts--;
                        if(attempts == 3){
                            switch_hints.setEnabled(false);
                        }
                    } else {
                        tvResult.setTextColor(Color.RED);
                        tvResult.setText("WRONG!");
                        switch_hints.setEnabled(false);
                        attempts--;
                    }
                }
            } else {
                generateQuestion();
            }


        }
        else if(v.getId()==R.id.btn_del){
            tvQuestion.setText(questionString.split("=")[0].trim() + " = " );
        }
        else if(v.getId()==R.id.btn_minus){
            String question = tvQuestion.getText().toString();
            String[] tempArr = question.split("=");
            if(!question.endsWith("?") && !tempArr[1].trim().startsWith("-")){
                tvQuestion.setText(tempArr[0].trim() + " = " + "-" + tempArr[1].trim());
            }


        }else {
            int enteredNum = Integer.parseInt(v.getTag().toString());

            if(tvQuestion.getText().toString().endsWith("?"))
                tvQuestion.setText((questionString.split("=")[0]).trim() + " = "+enteredNum);
            else
                tvQuestion.append(""+enteredNum);
        }
    }

    public void saveGame(){
        GameState gameState = new GameState();

        gameState.setTotalScore(totalScore);
        gameState.setAnswer(answer);
        gameState.setAttempts(attempts);
        gameState.setHintsEnabled(hintsEnabled);
        gameState.setLevel(level);
        gameState.setQuestion(questionString);
        gameState.setQuestionNumber(questionNumber);
        gameState.setTimeRemaining(timeRemaining);
        gameState.setResult(tvResult.getText().toString());

        FileOutputStream fos;
        ObjectOutputStream os;
        File file = new File(getApplicationContext().getFilesDir(), "gamedata.txt");

        try {
            fos = new FileOutputStream(file);
            System.out.println("File created!");
            os = new ObjectOutputStream(fos);
            os.writeObject(gameState);
            os.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void resumeGame(){

        GameState savedData;

        try{
            FileInputStream fis = getApplicationContext().openFileInput("gamedata.txt");
            ObjectInputStream is = new ObjectInputStream(fis);
            savedData = (GameState) is.readObject();
            is.close();
            fis.close();

            totalScore = savedData.getTotalScore();
            attempts = savedData.getAttempts();
            level = savedData.getLevel();
            timeRemaining = savedData.getTimeRemaining();
            questionNumber = savedData.getQuestionNumber();
            questionString = savedData.getQuestion();
            hintsEnabled = savedData.isHintsEnabled();

            tvResult.setText(savedData.getResult());

            tvScore.setText(totalScore+"");
            tvTime.setText(timeRemaining+"");
            tvQuestion.setText(questionString);

        } catch(IOException e){
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        timer = new CountDownTimer(timeRemaining*1000, 1000) {

            public void onTick(long millisUntilFinished) {
                int timeLeft = (int) (millisUntilFinished / 1000);
                timeRemaining = timeLeft;
                tvTime.setText(timeLeft + "");
            }

            public void onFinish() {

                timer = new CountDownTimer(11000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        int timeLeft = (int) (millisUntilFinished / 1000);
                        timeRemaining = timeLeft;
                        tvTime.setText(timeLeft + "");
                    }

                    public void onFinish() {
                        generateQuestion();
                    }
                };
                generateQuestion();
            }
        };
        timer.start();
    }

    public void resetResume(){
        GameState gameState = new GameState();

        gameState.setTotalScore(0);
        gameState.setAnswer(0);
        gameState.setAttempts(1);
        gameState.setHintsEnabled(false);
        gameState.setLevel(0);
        gameState.setQuestion("");
        gameState.setQuestionNumber(0);
        gameState.setTimeRemaining(0);
        gameState.setResult("");

        FileOutputStream fos;
        ObjectOutputStream os;
        File file = new File(getApplicationContext().getFilesDir(), "gamedata.txt");

        try {
            fos = new FileOutputStream(file);
            System.out.println("File created!");
            os = new ObjectOutputStream(fos);
            os.writeObject(gameState);
            os.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        resume = true;
        timer.cancel();
    }

    @Override
    public void onBackPressed() {

        timer.cancel();
        // set the title of the Alert Dialog
        alertDialogBuilder.setTitle("Exit Game");

        // set dialog message
        alertDialogBuilder
                .setMessage("Do you want to save the current game?")
                .setCancelable(false)
                .setPositiveButton("Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                saveGame();
                                GameActivity.this.finish();
                            }
                        })
                .setNegativeButton("No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                resetResume();
                                GameActivity.this.finish();
                            }
                        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    @Override
    public void onResume() {

        super.onResume();
        if(resume){
            timer = new CountDownTimer(timeRemaining*1000, 1000) {

                public void onTick(long millisUntilFinished) {
                    int timeLeft = (int) (millisUntilFinished / 1000);
                    timeRemaining = timeLeft;
                    tvTime.setText(timeLeft + "");
                }

                public void onFinish() {

                    timer = new CountDownTimer(11000, 1000) {

                        public void onTick(long millisUntilFinished) {
                            int timeLeft = (int) (millisUntilFinished / 1000);
                            timeRemaining = timeLeft;
                            tvTime.setText(timeLeft + "");
                        }

                        public void onFinish() {
                            generateQuestion();
                        }
                    };
                    generateQuestion();
                }
            };
            timer.start();
        }

    }
}
